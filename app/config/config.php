<?php
if(!defined('XAppInit')) {
	die('Direct access not permitted');
}


error_reporting(-1);
ini_set('display_errors', 'On');


// ===== Factory Default Dependancy Injection =====================
$di = new \Phalcon\Di\FactoryDefault();

// ===== Simple View and setting Volt engine ======================
use Phalcon\Mvc\View\Simple as SimpleView;
use Phalcon\Mvc\View\Engine\Volt;
//Register Volt as a service
$di->set('voltService', function($view, $di) {
	$volt = new Volt($view, $di);
	$volt->setOptions(array(
			"compiledPath" => function($templatePath) {
				return '../app/compiled-templates/' .basename($templatePath,'.volt')  . '.compiled';},
			"compiledExtension" => ".compiled",
			"compiledSeparator" => "_"
	));
	return $volt;
});
//Register Volt as template engine
$di->set('view', function() {
	$view = new SimpleView();
	$view->setViewsDir('../app/views/');
	$view->registerEngines(array(
		".volt" => 'voltService'
	));
	return $view;
});
		
// ===== MySQL Adapter ============================================
use Phalcon\Db\Adapter\Pdo\Mysql as MysqlAdapter;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Logger;

// Setup the database service
$di->setShared('db', function() {
	
	$connection = new MysqlAdapter(array(
		"host"		=> getenv('db_host'),
		"username"	=> getenv('db_user'),
		"password"	=> getenv('db_password'),
		"dbname"	=> getenv('db_name')
	
// 		"host"		=> "localhost",
// 		"username"	=> "root",
// 		"password"	=> "",
// 		"dbname"	=> "prana"
	));
// 	$eventsManager = new EventsManager();	
// 	$logger = new FileLogger("../app/logs/debug.log");
	
// 	// Listen all the database events
// 	$eventsManager->attach('db', function ($event, $connection) use ($logger) {
// 		if ($event->getType() == 'beforeQuery') {
// 			$logger->log($connection->getSQLStatement(), Logger::INFO);
// 		}
// 	});	
// 	// Assign the eventsManager to the db adapter instance
// 	$connection->setEventsManager($eventsManager);
	
	return $connection;
	
});

// ===== Class loaders ============================================
$loader = new \Phalcon\Loader();
// Register some classes
// register autoloader
$loader->registerDirs(
	array(
		"../app/Models/"
	)
);
$loader->register();

// ===== Cache Table MetaData =====================================
$di['modelsMetadata'] = function() {
	$metaData = new \Phalcon\Mvc\Model\Metadata\Files(array(
			'metaDataDir' => '../app/cache500/'
	));
	return $metaData;
};

$di->setShared('session', function() {
	$session = new Phalcon\Session\Adapter\Files();
	$session->start();
	return $session;
});


// ===== Initiate Micro Application ===============================
$app = new Phalcon\Mvc\Micro();
$app->setDI($di);


// ===== Initiate Basic JSON Response =============================
use Phalcon\Http\Response;
function jsonResponse($status_code, $responseData) {
	//Create a response
	$response = new Response();
	$response->setContentType('application/json', 'UTF-8');
	$response->setStatusCode($status_code);
	$response->setJsonContent($responseData);
	return $response;
}

$di->set('xconfig', function () {
	$data = new stdClass;
	$data->domain = 'http://prana.azurewebsites.net';
	return $data;
});

// ===== Not Found Scenario =======================================
$app->notFound(function () use ($app) {
	$app->response->setStatusCode(404, "Not Found")->sendHeaders();
	echo 'This is crazy, but this page was not found!';
});

